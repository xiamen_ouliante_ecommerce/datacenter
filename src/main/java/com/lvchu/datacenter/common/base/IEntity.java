package com.lvchu.datacenter.common.base;

import java.util.Date;

/**
 * 数据库实体的接口。
 *
 * @author wangruiv
 * @date 2017-08-29 16:10:37
 */
public interface IEntity {
	String getId();

	void setId(String id);

	Integer getDr();

	void setDr(Integer dr);

	Date getTs();

	void setTs(Date ts);

	String getCreator();

	void setCreator(String creator);

	Date getCreationTime();

	void setCreationTime(Date creationTime);

	String getModifier();

	void setModifier(String modifier);

	Date getModifiedTime();

	void setModifiedTime(Date modifiedTime);

	String getPersistStatus();

	void setPersistStatus(String persistStatus);
}
