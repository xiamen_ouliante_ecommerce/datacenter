package com.lvchu.datacenter.common.base;

import com.lvchu.datacenter.common.enums.ResponseEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author kc
 * @date 2019-11-12 9:28
 */
@Data
@Builder
@ApiModel(value = "Result对象", description = "固定响应对象")
public class Result<T> {
    @ApiModelProperty("响应码")
    private Integer code;
    @ApiModelProperty("响应消息")
    private String msg;
    @ApiModelProperty("响应结果")
    private T data;
    @ApiModelProperty("是否成功")
    private boolean success;

    public boolean isSuccess() {
        this.success = code.equals(ResponseEnum.CODE_0.getCode());
        return success;
    }

    private static Result build(ResponseEnum responseEnum) {
        return new ResultBuilder<>().code(responseEnum.getCode()).msg(responseEnum.getMessage()).build();
    }

    private static Result buildMsg(ResponseEnum responseEnum, String msg) {
        return new ResultBuilder<>().code(responseEnum.getCode()).msg(msg).build();
    }

    private static <T> Result<T> build(ResponseEnum responseEnum, T data) {
        return new ResultBuilder<T>().code(responseEnum.getCode()).msg(responseEnum.getMessage()).data(data).build();
    }

    private static <T>Result<T> build(ResponseEnum responseEnum,String msg,T data){
        return new ResultBuilder<T>().code(responseEnum.getCode()).msg(msg).data(data).build();
    }


    public static <T> Result<T> ok(ResponseEnum responseEnum,String msg,T data) {
        return build(responseEnum,msg,data);
    }
    /**
     * 响应成功
     *
     * @return 无响应对象的成功响应
     */
    public static Result ok() {
        return build(ResponseEnum.CODE_0);
    }





    /**
     * 响应成功
     *
     * @param message 响应消息
     * @return 无响应对象的成功响应
     */
    public static Result okMsg(String message) {
        return buildMsg(ResponseEnum.CODE_0, message);
    }

    /**
     * 响应成功
     *
     * @param data 响应对象
     * @return 包含响应成功的响应对象
     */
    public static <T> Result<T> ok(T data) {
        return build(ResponseEnum.CODE_0, data);
    }

    /**
     * 系统错误
     *
     * @return 响应为系统错误
     */
    public static Result failed() {
        return build(ResponseEnum.CODE_9);
    }

    /**
     * 系统错误
     *
     * @param message 错误消息
     * @return 响应为系统错误
     */
    public static Result failed(String message) {
        return buildMsg(ResponseEnum.CODE_9, message);
    }

    /**
     * 登录超时
     *
     * @return 响应登录超时
     */
    public static Result loginTimeout() {
        return build(ResponseEnum.CODE_10);
    }

    /**
     * 系统错误
     *
     * @param data 错误数据
     * @return 响应为系统错误
     */
    public static <T> Result<T> failedData(T data) {
        return build(ResponseEnum.CODE_9, data);
    }

    /**
     * 自定义异常响应
     *
     * @param responseEnum 响应枚举
     * @param message      响应消息
     * @return 响应为自定义错误
     */
    public static Result failed(ResponseEnum responseEnum, String message) {
        return buildMsg(responseEnum, message);
    }
}
