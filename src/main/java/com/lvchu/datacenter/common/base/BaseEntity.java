package com.lvchu.datacenter.common.base;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 * 实体类的抽象基类
 *
 * @author wangruiv
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable, IEntity {
    public static String DR_FIELD_NAME = "dr";

    public static String CREATE_TIME_FIELD_NAME = "creationTime";

    public static String PRIMARY_KEY_FIELD_NAME = "id";

    @Id
    @Column(name = "ID")
    @Size(max = 40)
    protected String id;

    @Column(name = "DR")
    protected Integer dr = 0;

    @Column(name = "TS")
    protected Date ts;

    @Column(name = "CREATOR")
    @Size(max = 40)
    protected String creator;

    @Column(name = "CREATION_TIME")
    protected Date creationTime;

    @Column(name = "MODIFIER")
    @Size(max = 40)
    protected String modifier;

    @Column(name = "MODIFIED_TIME")
    protected Date modifiedTime;

    @Transient
    protected String persistStatus = PersistStatus.UNCHANGED;

    /**
     * 数据库中的原值。
     */
    @Transient
    @JsonIgnore
    @JSONField(serialize = false)
    protected BaseEntity oldValue;

    /**
     * 规则提示信息
     */
    @Transient
    protected String promptMessage;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Integer getDr() {
        return dr;
    }

    @Override
    public void setDr(Integer dr) {
        this.dr = dr;
    }

    @Override
    public Date getTs() {
        return ts;
    }

    @Override
    public void setTs(Date ts) {
        this.ts = ts;
    }

    @Override
    public String getCreator() {
        return creator;
    }

    @Override
    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Override
    public Date getCreationTime() {
        return creationTime;
    }

    @Override
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String getModifier() {
        return modifier;
    }

    @Override
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    @Override
    public Date getModifiedTime() {
        return modifiedTime;
    }

    @Override
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @Override
    public String getPersistStatus() {
        return persistStatus;
    }

    @Override
    public void setPersistStatus(String persistStatus) {
        this.persistStatus = persistStatus;
    }

    public BaseEntity getOldValue() {
        return oldValue;
    }

    public void setOldValue(BaseEntity oldValue) {
        this.oldValue = oldValue;
    }

    public String getPromptMessage() {
        return promptMessage;
    }

    public void setPromptMessage(String promptMessage) {
        this.promptMessage = promptMessage;
    }
}
