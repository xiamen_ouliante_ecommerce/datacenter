package com.lvchu.datacenter.common.shiro.realms;

import com.lvchu.datacenter.common.shiro.LoginVO;
import com.lvchu.datacenter.common.shiro.ShiroUtils;
import com.lvchu.datacenter.common.shiro.multi.CustomLoginToken;
import com.lvchu.datacenter.common.shiro.multi.UserRealmEnum;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import java.util.Arrays;

/**
 * @author kc
 * @date 2020-07-10 9:04
 */
public class UserRealm extends AuthorizingRealm {


    @Override
    public String getName() {
        return UserRealmEnum.ORDERING_PLATFORM.getRealmName();
    }

    public UserRealm(CacheManager cacheManager) {
        super(cacheManager);
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return new SimpleAuthorizationInfo();
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        CustomLoginToken loginToken = (CustomLoginToken) token;

        String name = "orderingPlatform";
        String versionPassword = "orderingPlatform";
        String username = loginToken.getUsername();
        String password = Arrays.toString(loginToken.getPassword());

        if (!name.equals(username) || !versionPassword.equals(password)) {
            throw new IncorrectCredentialsException("账号或密码错误");
        }

        return new SimpleAuthenticationInfo();
    }

    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        HashedCredentialsMatcher shaCredentialsMatcher = new HashedCredentialsMatcher();
        shaCredentialsMatcher.setHashAlgorithmName(ShiroUtils.HASH_ALGORITHM_NAME);
        shaCredentialsMatcher.setHashIterations(ShiroUtils.HASH_ITERATIONS);
        super.setCredentialsMatcher(shaCredentialsMatcher);
    }

    @Override
    public void onLogout(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
        LoginVO shiroUser = ShiroUtils.getUser();
        removeUserCache(shiroUser);
    }

    /**
     * 清除用户缓存
     *
     * @param shiroUser 用户信息
     */
    private void removeUserCache(LoginVO shiroUser) {
        removeUserCache(shiroUser.getMobile());
    }

    /**
     * 清除用户缓存
     *
     * @param loginName 账号
     */
    private void removeUserCache(String loginName) {
        SimplePrincipalCollection principals = new SimplePrincipalCollection();
        principals.add(loginName, super.getName());
        super.clearCachedAuthenticationInfo(principals);
    }
}
