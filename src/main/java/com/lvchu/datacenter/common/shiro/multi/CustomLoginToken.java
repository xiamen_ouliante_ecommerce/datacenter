package com.lvchu.datacenter.common.shiro.multi;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * 自定义shiro-token
 *
 * @author kc
 * @date 2019-05-06 10:15
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomLoginToken extends UsernamePasswordToken {
    private String loginType;

    /**
     * 账号登录
     *
     * @param username 用户名
     * @param password 密码
     */
    public CustomLoginToken(String username, String password) {
        super(username, password);
        this.loginType = UserRealmEnum.NORMAL.getRealmName();
    }

}
