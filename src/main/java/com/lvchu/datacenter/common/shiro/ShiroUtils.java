package com.lvchu.datacenter.common.shiro;

import com.alibaba.fastjson.JSON;
import com.lvchu.datacenter.common.enums.ResponseEnum;
import com.lvchu.datacenter.common.exception.BusinessException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;


/**
 * Shiro工具类
 *
 * @author lixin
 * @since 2019-2-14
 */
public class ShiroUtils {
    /**
     * 加密算法
     */
    public final static String HASH_ALGORITHM_NAME = "SHA-256";
    /**
     * 循环次数
     */
    public final static int HASH_ITERATIONS = 16;


    public static String sha256(String password, String salt) {
        return new SimpleHash(HASH_ALGORITHM_NAME, password, salt, HASH_ITERATIONS).toString();
    }

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }


    public static boolean isLogin() {
        return SecurityUtils.getSubject().getPrincipal() != null;
    }

    public static void logout() {
        SecurityUtils.getSubject().logout();
    }

    /**
     * 获取登录用户信息，未登录时为null
     *
     * @return 用户信息
     */
    public static LoginVO getUserWithNull() {
        return getShiroUser();
    }

    private static LoginVO getShiroUser() {
        Object obj = getSubject().getPrincipal();
        if (obj != null) {
            LoginVO user;
            if (obj instanceof LoginVO) {
                user = (LoginVO) obj;
            } else {
                user = JSON.parseObject(JSON.toJSON(obj).toString(), LoginVO.class);
            }
            return user;
        }
        return null;
    }


    /**
     * 获取登录用户信息，未登录时抛出异常
     *
     * @return 用户信息
     */
    public static LoginVO getUser() {
        LoginVO user = getShiroUser();
        if (user != null) {
            return user;
        }
        throw new BusinessException(ResponseEnum.CODE_5);
    }

    /**
     * 获取主键
     *
     * @return 主键
     */
    public static String getPk() {
        return getUser().getPkPsndoc();
    }
}

