package com.lvchu.datacenter.common.shiro;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 登录返回对象
 *
 * @author lixin
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("登录用户返回信息")
public class LoginVO implements Serializable {

    @ApiModelProperty(value = "人员信息主键")
    private String pkPsndoc;

    @ApiModelProperty("职位")
    private String w;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("公司id")
    private List<String> pkOrg;

    @ApiModelProperty(value = "登录token")
    private String token;

}
