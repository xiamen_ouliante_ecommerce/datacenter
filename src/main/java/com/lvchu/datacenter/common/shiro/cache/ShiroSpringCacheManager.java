package com.lvchu.datacenter.common.shiro.cache;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.util.Destroyable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 使用spring-cache作为shiro缓存
 * 缓存管理器
 *
 * @author lixin
 * @since 2019-2-14
 */
@Slf4j
public class ShiroSpringCacheManager implements CacheManager, Destroyable {
    private static final Logger logger = LoggerFactory.getLogger(ShiroSpringCacheManager.class);
    private org.springframework.cache.CacheManager cacheManager;

    @Autowired
    public void setCacheManager(org.springframework.cache.CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        log.debug("Acquiring ShiroSpringCache instance named [{}]", name);
        org.springframework.cache.Cache cache = cacheManager.getCache(name);
        return new ShiroSpringCache<>(cache);
    }

    @Override
    public void destroy() {
        cacheManager = null;
    }

}
