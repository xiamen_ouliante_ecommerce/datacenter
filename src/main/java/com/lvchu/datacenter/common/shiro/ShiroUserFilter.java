package com.lvchu.datacenter.common.shiro;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.web.filter.authc.UserFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * shiro 用户登录过滤器
 *
 * @author lixin
 * @since 2019-2-18
 */
@Slf4j
public class ShiroUserFilter extends UserFilter {
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        if (WebUtils.toHttp(request).getMethod().equalsIgnoreCase(RequestMethod.OPTIONS.name())) {
            return true;
        }
        //返回未登录json字符串
        WebUtils.toHttp(response).setHeader("Access-Control-Allow-Origin","*");
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write("{\"code\":\"5\",\"msg\":\"未登录\",\"success\":false}");
        response.getWriter().flush();
        response.getWriter().close();
        return false;
    }

    @Override
    public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        if (WebUtils.toHttp(request).getMethod().equalsIgnoreCase(RequestMethod.OPTIONS.name())) {
            //跨域请求允许访问
            return true;
        }
        return super.onPreHandle(request, response, mappedValue);
    }
}
