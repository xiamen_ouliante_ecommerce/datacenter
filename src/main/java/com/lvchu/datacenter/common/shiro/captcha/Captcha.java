package com.lvchu.datacenter.common.shiro.captcha;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * @author kc
 * @date 2019-04-29 15:05
 */
@Component
@Data
public class Captcha implements InitializingBean {
    private Cache dreamCaptchaCache;
    @Resource
    private CacheManager cacheManager;
    private String cacheName;
    private final static String DEFAULT_CHACHE_NAME = "captchaCache";

    public Captcha() {
        this.cacheName = DEFAULT_CHACHE_NAME;
    }

    /**
     * 生成验证码
     */
    public String generate(HttpServletResponse response, Long timestamp) {
        // 转成大写重要
        String captchaCode = CaptchaUtils.generateCode().toUpperCase();
        // 生成验证码
        CaptchaUtils.generate(response, captchaCode);
        dreamCaptchaCache.put(timestamp + "", captchaCode);
        return captchaCode;
    }

    /**
     * 检验验证码
     *
     * @param captchaCode 验证码code
     * @param captcha     验证码
     * @return 是否通过
     */
    public boolean checkCaptcha(Long captchaCode, String captcha) {
        String captchaCache = dreamCaptchaCache.get(captchaCode,String.class);
        return StringUtils.equalsIgnoreCase(captchaCache, captcha);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(cacheManager, "cacheManager must not be null!");
        Assert.hasText(cacheName, "cacheName must not be empty!");
        this.dreamCaptchaCache = cacheManager.getCache(cacheName);
    }
}
