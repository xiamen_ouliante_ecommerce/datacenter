package com.lvchu.datacenter.common.shiro;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;

/**
 * 自定义token获取
 *
 * @author lixin
 * @since 2019-2-16
 */
public class CustomDefaultWebSessionManager extends DefaultWebSessionManager {

    /**
     *  
     * 获取session id
     * 前后端分离将从请求头中获取jsesssionid
     */
    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
        // 从请求头中获取token
        String token = WebUtils.toHttp(request).getHeader("x-token");
        if (StringUtils.isBlank(token)) {
            // 从请求参数中获取token
            token = WebUtils.toHttp(request).getParameter("token");
            if (StringUtils.isNoneBlank(token)){
                //设置响应cookie
                getSessionIdCookie().setValue(token);
                getSessionIdCookie().saveTo(WebUtils.toHttp(request), WebUtils.toHttp(response));
            }
        }
        // 判断是否有值
        if (StringUtils.isNoneBlank(token)) {
            // 设置当前session状态
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_SOURCE, "url");
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID, token);
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_IS_VALID, Boolean.TRUE);
            return token;
        }

        // 若header获取不到token则尝试从cookie中获取
        return super.getSessionId(request, response);
    }
}
