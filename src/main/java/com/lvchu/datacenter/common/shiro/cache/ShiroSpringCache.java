package com.lvchu.datacenter.common.shiro.cache;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.cache.CacheException;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * 使用spring-cache作为shiro缓存
 * @author lixin
 * @since 2019-2-14
 */
@Slf4j
public class ShiroSpringCache<K, V> implements org.apache.shiro.cache.Cache<K, V> {
	private final Cache cache;
	
	ShiroSpringCache(Cache cache) {
		if (cache == null) {
			throw new IllegalArgumentException("Cache argument cannot be null.");
		}
		this.cache = cache;
	}

	@Override
	public V get(K key) throws CacheException {
		log.debug("Getting Cache [{}]:{}", cache.getName(), key);
		ValueWrapper valueWrapper = cache.get(key);
		if (valueWrapper == null) {
			log.debug("Element for [{}] is null.", key);
			return null;
		}
		return (V) valueWrapper.get();
	}

	@Override
	public V put(K key, V value) throws CacheException {
		log.debug("Putting Cache [{}]:{}", cache.getName(), key);
		V previous = get(key);
		cache.put(key, value);
		return previous;
	}

	@Override
	public V remove(K key) throws CacheException {
		log.debug("Removing Cache [{}]:{}", cache.getName(), key);
		V previous = get(key);
		cache.evict(key);
		return previous;
	}

	@Override
	public void clear() throws CacheException {
		log.debug("Clearing all Cache [{}]", cache.getName());
		cache.clear();
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public Set<K> keys() {
		return Collections.emptySet();
	}

	@Override
	public Collection<V> values() {
		return Collections.emptySet();
	}

	@Override
	public String toString() {
		return "ShiroSpringCache [" + this.cache.getName() + "]";
	}
}
