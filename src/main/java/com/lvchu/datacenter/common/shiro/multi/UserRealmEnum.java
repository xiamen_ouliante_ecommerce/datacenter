package com.lvchu.datacenter.common.shiro.multi;

/**
 * @author kc
 * @date 2019-05-06 10:17
 */
public enum UserRealmEnum {
    /**
     * 前端登陆
     */
    NORMAL("userRealm"),

    /**
     * 订货平台
     */
    ORDERING_PLATFORM("ORDERING_PLATFORM");

    ;
    private String realmName;

    UserRealmEnum(String realmName) {
        this.realmName = realmName;
    }

    public String getRealmName() {
        return realmName;
    }
}
