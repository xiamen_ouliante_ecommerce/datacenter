package com.lvchu.datacenter.common.exception;

import com.lvchu.datacenter.common.enums.ResponseEnum;
import org.springframework.util.StringUtils;

/**
 * 统一异常处理器
 *
 * @author kc
 * @date 2020-05-06 11:14
 */
public class BusinessException extends RuntimeException {
    /**
     * 异常码
     */
    private ResponseEnum responseEnum;
    /**
     * 异常消息
     */
    private String msg;

    public ResponseEnum getResponseEnum() {
        return responseEnum;
    }

    public void setResponseEnum(ResponseEnum responseEnum) {
        this.responseEnum = responseEnum;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private static String buildErrMsg(ResponseEnum responseEnum) {
        return buildErrMsg(responseEnum, null);
    }

    private static String buildErrMsg(ResponseEnum responseEnum, String msg) {
        return (!StringUtils.isEmpty(msg) ? msg : responseEnum.getMessage()) + "[" + responseEnum.getCode() + "]";
    }

    /**
     * 99999 系统异常
     */
    public BusinessException() {
        super(buildErrMsg(ResponseEnum.CODE_9));
        this.responseEnum = ResponseEnum.CODE_9;
        this.msg = ResponseEnum.CODE_9.getMessage();
    }

    /**
     * 99999 系统异常
     *
     * @param message 自定义异常消息
     */
    public BusinessException(String message) {
        super(buildErrMsg(ResponseEnum.CODE_9, message));
        this.responseEnum = ResponseEnum.CODE_9;
        this.msg = message;
    }

    /**
     * 99999 系统异常
     *
     * @param message 自定义异常消息
     * @param cause   异常
     */
    public BusinessException(String message, Throwable cause) {
        super(cause);
        this.responseEnum = ResponseEnum.CODE_9;
        this.msg = message;
    }

    /**
     * 其它异常
     *
     * @param responseEnum 异常枚举
     */
    public BusinessException(ResponseEnum responseEnum) {
        super(buildErrMsg(responseEnum));
        this.responseEnum = responseEnum;
        this.msg = responseEnum.getMessage();
    }

    /**
     * 其它异常
     *
     * @param responseEnum 异常枚举
     * @param message      异常消息
     */
    public BusinessException(ResponseEnum responseEnum, String message) {
        super(buildErrMsg(responseEnum, message));
        this.responseEnum = responseEnum;
        this.msg = message;
    }
}
