package com.lvchu.datacenter.common.exception;


import com.lvchu.datacenter.common.base.Result;
import com.lvchu.datacenter.common.enums.ResponseEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.UnexpectedTypeException;

/**
 * 异常处理器
 *
 * @author lixin
 * @since 2019-2-13
 */
@Slf4j
@RestControllerAdvice
public class SystemExceptionHandler {

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result processValidationError(MissingServletRequestParameterException ex) {
        log.debug(ex.getMessage(), ex);
        return Result.failed(ResponseEnum.CODE_8, ex.getMessage());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, UnexpectedTypeException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result processValidationError(Exception ex) {
        log.debug(ex.getMessage(), ex);
        if (ex instanceof MethodArgumentNotValidException) {
            BindingResult result = ((MethodArgumentNotValidException) ex).getBindingResult();
            FieldError error = result.getFieldError();

            return getFieldErrorResult(error);
        } else {
            return Result.failed(ResponseEnum.CODE_8, ex.getMessage());
        }
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result processException(BindException ex) {
        log.debug(ex.getMessage(), ex);
        FieldError error = ex.getFieldError();
        return getFieldErrorResult(error);
    }

    /**
     * 对hibernate-validator异常错误信息简单处理
     *
     * @param error 参数校验异常
     * @return 校验异常响应
     */
    private Result getFieldErrorResult(FieldError error) {
        String errorMsg = "参数错误";
        if (error != null) {
            errorMsg = error.getField() + error.getDefaultMessage();
        }
        return Result.failed(ResponseEnum.CODE_8, errorMsg);
    }

    @ExceptionHandler(BusinessException.class)
    public Result processException(BusinessException ex) {
        log.error(ex.getMsg(), ex);
        return Result.failed(ex.getResponseEnum(), ex.getMsg());
    }

    @ExceptionHandler({UnauthorizedException.class, UnauthenticatedException.class})
    public Result processException() {
        return Result.failed("抱歉，您没有访问权限");
    }

    @ExceptionHandler(Exception.class)
    public Result otherException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return Result.failed("系统异常");
    }
}
