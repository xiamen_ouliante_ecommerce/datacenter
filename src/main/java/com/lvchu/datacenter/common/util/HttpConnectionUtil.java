package com.lvchu.datacenter.common.util;

import com.google.gson.Gson;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.io.*;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author lz
 * @date 2020-10-30 9:20
 */
public class HttpConnectionUtil {
    public static URLConnection openConnection(String path) {
        URLConnection conn;
        try {
            URL url = new URL(path);
            conn = url.openConnection();
            if (conn instanceof HttpURLConnection) {
                ((HttpURLConnection) conn).setChunkedStreamingMode(2048);
            }
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("content-type", "application/x-java-serialized-object,charset=utf-8");
        } catch (Exception e) {
            throw new RuntimeException("connection error");
        }
        return conn;
    }

    public static JSONObject transferDataToServer(Object data, URLConnection conn) {

        StringBuffer returndata = new StringBuffer();

        ObjectOutputStream oos = null;
        Gson gson = new Gson();
        String requestStr = gson.toJson(data);
        OutputStream out = null;
        ObjectInputStream in = null;

        try {
            out = conn.getOutputStream();
            oos = new ObjectOutputStream(out);
            oos.writeObject(requestStr);
            oos.flush();
            in = new ObjectInputStream(conn.getInputStream());
            return JSONObject.fromObject(in.readObject());

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            closeIO(oos, out, in);
        }
    }


    /**
     * 流关闭
     *
     * @param oos
     * @param out
     * @param in
     */
    private static void closeIO(ObjectOutputStream oos, OutputStream out, ObjectInputStream in) {

        if (oos != null) {
            try {
                oos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static Object callBackData(URLConnection conn) {
        Object obj;
        ObjectInputStream ois = null;
        InputStream is = null;
        try {
            is = conn.getInputStream();
            ois = new ObjectInputStream(is);
            obj = ois.readObject();
        } catch (Exception e) {
            throw new RuntimeException("读取返回数据错误：", e);
        } finally {
            if (null != ois) {
                try {
                    ois.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return obj;
    }

    public static Object callBackData(URLConnection conn, Class classOfT) {
        Object obj;
        ObjectInputStream ois = null;
        InputStream is = null;
        try {
            is = conn.getInputStream();
            ois = new ObjectInputStream(is);
            obj = ois.readObject();
            Gson gson = new Gson();
            return gson.fromJson((String) obj, classOfT);
        } catch (Exception e) {
            throw new RuntimeException("读取返回数据错误：", e);
        } finally {
            if (null != ois) {
                try {
                    ois.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static <T> T[] callBackDataArray(URLConnection conn, Class<T> classOfT) {
        Object obj = null;
        BufferedInputStream ois = null;
        InputStream is = null;
        try {
            is = conn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String tempLine;
            StringBuffer resultBuffer = new StringBuffer();
            String s;
            while ((tempLine = reader.readLine()) != null) {
                resultBuffer.append(tempLine);
            }
            s = resultBuffer.toString();

            JSONArray array = JSONArray.fromObject(s);
            T[] objs = (T[]) Array.newInstance(classOfT, array.size());
            for (int i = 0; i < array.size(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                objs[i] = (T) JSONObject.toBean(jsonObject, classOfT);
            }

            return objs;
        } catch (Exception e) {
            throw new RuntimeException("读取返回数据错误：", e);
        } finally {
            if (null != ois) {
                try {
                    ois.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static <T> T[] convert(Object obj, Class<T> classOfT) {
        JSONArray array = JSONArray.fromObject(obj);
        T[] objs = (T[]) Array.newInstance(classOfT, array.size());
        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonObject = array.getJSONObject(i);
            objs[i] = (T) JSONObject.toBean(jsonObject, classOfT);
        }
        return objs;
    }
}
