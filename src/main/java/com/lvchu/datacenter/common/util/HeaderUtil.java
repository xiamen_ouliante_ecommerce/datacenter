package com.lvchu.datacenter.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Utility class for HTTP headers creation.
 */
public final class HeaderUtil {
    private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

    private static final String APPLICATION_NAME = "OCC";

    private static final String CODE_INFO = "1";
    private static final String CODE_WARN = "2";
    private static final String CODE_ERROR = "3";

    public static final String MESSAGE_HEADER_NAME = "X-OCC-message";

    public static final String PARAMS_HEADER_NAME = "X-OCC-params";

    public static final String CODE_HEADER_NAME = "X-OCC-code";

    private HeaderUtil() {
    }

    public static HttpHeaders createAlert(String message, String param) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-OCC-message", message);
        headers.add("X-OCC-params", param);
        return headers;
    }

    public static HttpHeaders createAlert(String message, String param, String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-OCC-message", message);
        headers.add("X-OCC-params", param);
        headers.add("X-OCC-code", code);
        return headers;
    }

    public static <T> ResponseEntity<T> createOkEntity(String msg, String code, T body) {
        try {
            msg = (msg == null) ? "" : URLEncoder.encode(msg, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage());
        }
        return ResponseEntity.ok().headers(createAlert(msg, null, code)).body(body);
    }

    public static <T> ResponseEntity<T> createInfoResponse(String msg, T body) {
        return createOkEntity(msg, CODE_INFO, body);
    }

    public static <T> ResponseEntity<T> createWarnResponse(String msg, T body) {
        return createOkEntity(msg, CODE_WARN, body);
    }

    public static <T> ResponseEntity<T> createErrorResponse(String msg, T body) {
        return createOkEntity(msg, CODE_ERROR, body);
    }

    public static HttpHeaders createEntityCreationAlert(String entityName, String param) {
        return createAlert(APPLICATION_NAME + "." + entityName + ".created", param);
    }

    public static HttpHeaders createEntityUpdateAlert(String entityName, String param) {
        return createAlert(APPLICATION_NAME + "." + entityName + ".updated", param);
    }

    public static HttpHeaders createEntityDeletionAlert(String entityName, String param) {
        return createAlert(APPLICATION_NAME + "." + entityName + ".deleted", param);
    }

    public static HttpHeaders createFailureAlert(String entityName, String errorKey, String defaultMessage) {
        log.error("Entity creation failed, {}", defaultMessage);

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-OCC-message", "error." + errorKey);
        headers.add("X-OCC-params", entityName);
        headers.add("X-OCC-code", "2");
        return headers;
    }

    public static <K> boolean hasError(ResponseEntity<K> res) {
        Assert.notNull(res, "ResponseEntity不能为空");
        return res.getHeaders().get(HeaderUtil.CODE_HEADER_NAME).contains(HeaderUtil.CODE_ERROR);
    }

    public static <K> String getErrorMsg(ResponseEntity<K> res) {
        HttpHeaders headers = res.getHeaders();
        if (headers.get(HeaderUtil.CODE_HEADER_NAME).contains(HeaderUtil.CODE_ERROR)) {
            String msg = headers.get(HeaderUtil.MESSAGE_HEADER_NAME) == null ? "" : headers.get(HeaderUtil.MESSAGE_HEADER_NAME).toString();
            try {
                return URLDecoder.decode(msg, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
