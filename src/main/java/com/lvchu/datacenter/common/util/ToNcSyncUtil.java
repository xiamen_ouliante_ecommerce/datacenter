package com.lvchu.datacenter.common.util;

import com.lvchu.datacenter.common.config.IntegrationNcConfig;
import com.lvchu.datacenter.domain.dto.ToNcRequestDto;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URLConnection;

/**
 * NC集成工具类。
 *
 * @author wangruiv
 */
@Component
public class ToNcSyncUtil {
    @Autowired
    private IntegrationNcConfig integrationNcConfig;
    
    public IntegrationNcConfig getIntegrationNcConfig() {
		return integrationNcConfig;
	}
	public void setIntegrationNcConfig(IntegrationNcConfig integrationNcConfig) {
		this.integrationNcConfig = integrationNcConfig;
	}
	private static Logger logger = LoggerFactory.getLogger(ToNcSyncUtil.class.getName());
    /**
     * 向NC发送数据。
     *
     * @param data 要发送的数据
     * @param businessModule 业务模块
     * @param operation 业务操作
     */
    public JSONObject send(Object data, String businessModule, String operation) {
        URLConnection urlConnection = HttpConnectionUtil.openConnection(integrationNcConfig.getSyncUrl());
        logger.info("data: " + data + "       " + "businessModule:" + businessModule + "      " + "operation: " + operation);
        ToNcRequestDto toNcRequestDto = new ToNcRequestDto(integrationNcConfig);
        toNcRequestDto.build(data, businessModule, operation);
        JSONObject rdata = HttpConnectionUtil.transferDataToServer(toNcRequestDto, urlConnection);
        if("400".equals(rdata.get("resultCode"))){
            throw new RuntimeException("同步错误："+rdata.get("errorMsg"));
        }
        return rdata;

    }
}
