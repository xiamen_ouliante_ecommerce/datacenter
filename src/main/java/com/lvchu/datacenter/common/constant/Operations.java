package com.lvchu.datacenter.common.constant;

/**
 * 外系统同步，operation类型
 *
 * @author douhe
 */
public interface Operations {
    /**
     * 新增
     */
    public static final String CREATE = "create";
    /**
     * 修改
     */
    public static final String UPDATE = "update";
    /**
     * 启用
     */
    public static final String ENABLE = "enable";
    /**
     * 停用
     */
    public static final String UNENABLE = "unenable";
    /**
     * 删除
     */
    public static final String DELETE = "delete";

    /**
     * 冻结
     */
    public static final String FREEZE = "freeze";
    /**
     * 取消冻结
     */
    public static final String UNFREEZE = "unfreeze";
    /**
     * 合并后
     */
    public static final String MERGE = "merge";
    /**
     * 批改后
     */
    public static final String BATCHUPDATE = "batchupdate";
    /**
     * 升级后
     */
    public static final String UPGRADE = "upgrade";

    /**
     * 打开
     */
    public static final String OPEN = "open";

    /**
     * 关闭
     */
    public static final String CLOSE = "close";

    /**
     * 行打开
     */
    public static final String OPEN_LINE = "open_line";

    /**
     * 行关闭
     */
    public static final String CLOSE_LINE = "close_line";

    /**
     * 订单发货打开
     */
    public static final String DELIVERY_OPEN = "delivery_open";

    /**
     * 订单发货关闭
     */
    public static final String DELIVERY_CLOSE = "delivery_close";

    /**
     * 订单发货行打开
     */
    public static final String DELIVERY_OPEN_LINE = "delivery_open_line";

    /**
     * 订单发货行关闭
     */
    public static final String DELIVERY_CLOSE_LINE = "delivery_close_line";
    /**
     * 同步销售订单
     */
    public static final String SYN_SALE_ORDER = "syn_sale_order";

}
