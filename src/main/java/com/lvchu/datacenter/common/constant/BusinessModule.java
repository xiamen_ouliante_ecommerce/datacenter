package com.lvchu.datacenter.common.constant;

/**
 * 业务模块常量。
 *
 * @author wangruiv
 */
public interface BusinessModule {
    /**
     * 目标为NC的客户。
     */
    String TO_NC_CUSTOMER = "to-nc-customer";

    /**
     * 目标为NC的物料。
     */
    String TO_NC_MATERIAL = "to-nc-material";
    /**
     * 目标为NC的销售出库单。
     */
    String TO_NC_SALE_OUT = "to-nc-sale-out";

    /**
     * 目标为NC的销售订单
     */
    String TO_NC_SALE_ORDER = "to-nc-sale-order";

    /**
     * 目标为NC的收款单
     */
    String TO_NC_BILL_RECEIPT = "to-nc-bill-receipt";

    /**
     * 目标为NC的转库入库订单
     */
    String TO_NC_TRANSFER_IN = "to-nc-transfer-in";

    /**
     * 目标为NC的转库出库订单
     */
    String TO_NC_TRANSFER_OUT = "to-nc-transfer-out";

    /**
     * 目标为NC的其它出库订单
     */
    String TO_NC_OTHER_OUT = "to-nc-other-out";

    /**
     * 目标为NC的其它入库订单
     */
    String TO_NC_OTHER_IN= "to-nc-other-in";

    /**
     * 目标为NC的调拨订单
     */
    String TO_NC_ALLOCATION = "to-nc-allocation";

    /**
     * 目标为NC的调拨入库订单
     */
    String TO_NC_ALLOCATION_IN = "to-nc-allocation-in";

    /**
     * 目标为NC的调拨出库订单
     */
    String TO_NC_ALLOCATION_OUT = "to-nc-allocation-out";

    /**
     * 目标为NC的采购入库订单
     */
    String TO_NC_PURCHASE_IN = "to-nc-purchase-in";

    /**
     * 目标为NC的采购退货出库订单
     */
    String TO_NC_PURCHASE_OUT = "to-nc-purchase-out";

    /**
     * 目标为NC的采购订单
     */
    String TO_NC_PURCHASE_ORDER = "to-nc-purchase-order";

    /**
     *目标为NC的订单与收款单关联关系
     */
    String TO_NC_ORDER_RECEIPT_ORDER_RELATION = "to-nc-order-receipt-order-relation";

    /**
     * 目标为NC的客户费用单
     */
    String TO_NC_CUSTOMER_BILL = "to-nc-customer-bill";

    /**
     * 目标为NC的客户费用申请单
     */
    String TO_NC_CUSTOMER_APPLY = "to-nc-customer-apply";

    /**
     * 目标为NC的助促销品申请单
     */
    String TO_NC_GIFT_APPLY = "to-nc-gift-apply";

    /**
     * 目标为NC的零售销售订单
     */
    String TO_NC_SALE_ORDER_GATHER = "to-nc-sale-order-gather";


    /**
     * 目标为NC的客户收货地址。
     */
    String TO_NC_CUSTOMER_ADDRESS = "to-nc-customer-address";
    /**
     * 目标为NC的客户开票信息。
     */
    String TO_NC_CUSTOMER_INVOICE = "to-nc-customer-invoice";
    /**
     * 目标为NC的客户账户。
     */
    String TO_NC_CUSTOMER_ACCOUNT = "to-nc-customer-account";
    /**
     * 目标为NC的客户联系人。
     */
    String TO_NC_CUSTOMER_CONTACT = "to-nc-customer-contact";
}
