package com.lvchu.datacenter.common.constant;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 系统常量
 *
 * @author kc
 * @date 2020-10-29 22:00
 */
@Component
public class Constant {
    @Resource
    private Environment environment;
    public static String APPLICATION_NAME;
    public static String ACTIVE_ENV;

    @PostConstruct
    public void init() {
        APPLICATION_NAME = environment.getProperty("spring.application.name", "data-center");
        ACTIVE_ENV = environment.getProperty("spring.profiles.active", "local");
    }

}
