package com.lvchu.datacenter.common.enums;

import lombok.Getter;

/**
 * @author kc
 * @date 2019-11-11 18:02
 */
@Getter
public enum ResponseEnum {
    /**
     * 0 返回成功
     */
    CODE_0(0, "返回成功"),
    /**
     * 5 未登录
     */
    CODE_5(5, "未登录"),
    /**
     * 8 参数有误
     */
    CODE_8(8, "参数有误"),
    /**
     * 9 发生错误
     */
    CODE_9(9, "发生错误"),
    /**
     * 10 登录超时
     */
    CODE_10(10, "登录超时"),
    ;
    private Integer code;
    private String message;

    ResponseEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

}
