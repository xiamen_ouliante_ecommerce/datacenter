package com.lvchu.datacenter.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * NC集成配置。
 *
 * @author wangruiv
 */
@Component
@ConfigurationProperties(prefix = "occ.integration.nc")
public class IntegrationNcConfig {
    private String usercode;

    private String pwd;

    private String datasource;

    private String syncUrl;
    
    private String syncBom;

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    public String getSyncUrl() {
        return syncUrl;
    }

    public void setSyncUrl(String syncUrl) {
        this.syncUrl = syncUrl;
    }

	public String getSyncBom() {
		return syncBom;
	}

	public void setSyncBom(String syncBom) {
		this.syncBom = syncBom;
	}

}
