package com.lvchu.datacenter.mq;


import com.lvchu.datacenter.common.constant.Constant;

/**
 * 消息队列主题配置
 *
 * @author kc
 */
public class RocketMqTopicNames {


    static String sendTopic(String topicName) {
        return Constant.APPLICATION_NAME + "-" + Constant.ACTIVE_ENV + "-" + topicName;
    }
}
