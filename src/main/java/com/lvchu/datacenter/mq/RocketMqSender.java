package com.lvchu.datacenter.mq;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;


/**
 * @author kc
 */
@Slf4j
@Component
public class RocketMqSender {
    @Resource
    private RocketMQTemplate rocketMqTemplate;

    private static RocketMQTemplate staticRocketMQTemplate;


    @PostConstruct
    public void init() {
        staticRocketMQTemplate = rocketMqTemplate;
    }

    /**
     * 发送实时mq
     *
     * @param topicName   主题
     * @param messageBody 消息
     */
    public static void convertAndSend(String topicName, Object messageBody) {
        long startTime = System.currentTimeMillis();
        String fullTopicName = RocketMqTopicNames.sendTopic(topicName);
        log.debug("开始发送消息【{}】到【{}】", messageBody, fullTopicName);
        try {
            staticRocketMQTemplate.convertAndSend(fullTopicName, messageBody);
            log.debug("发送消息【{}】到【{}】 成功,用时 {} ms;", messageBody, fullTopicName, System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            log.debug("发送消息【{}】到【{}】 失败", messageBody, fullTopicName, e);
        }
    }

    /**
     * 发送延迟消息
     *
     * @param topicName   主题
     * @param messageBody 消息
     * @param timeout     发送超时时间 毫秒
     * @param delayLevel  rocket延时配置序号
     */
    public static void syncSend(String topicName, Object messageBody, long timeout, int delayLevel) {
        long startTime = System.currentTimeMillis();
        String fullTopicName = RocketMqTopicNames.sendTopic(topicName);
        log.debug("开始发送延迟消息【{}】到【{}】", messageBody, fullTopicName);
        try {
            Message message = staticRocketMQTemplate.getMessageConverter().toMessage(messageBody, null);
            assert message != null;
            SendResult sendResult = staticRocketMQTemplate.syncSend(fullTopicName, message, timeout, delayLevel);
            log.debug("发送延迟消息【{}】到【{}】 成功,用时 {} ms;\r\n{}", messageBody, fullTopicName, System.currentTimeMillis() - startTime, sendResult);
        } catch (Exception e) {
            log.debug("发送延迟消息【{}】到【{}】 失败", messageBody, fullTopicName, e);
        }
    }
}
