package com.lvchu.datacenter.mq;

import cn.hutool.core.util.TypeUtil;
import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;


/**
 * @author kc
 */
public abstract class BaseConsumer<T> implements RocketMQListener {
    protected Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void onMessage(Object message) {
        long startTime = System.currentTimeMillis();
        String topicName = RocketMqTopicNames.sendTopic(getTopicName());
        log.debug("消费【{}】【{}】消息: {} 开始", topicName, remark(), message);
        try {
            doMessage(JSON.parseObject(JSON.toJSON(message).toString(), getType()));
            log.debug("消费【{}】【{}】消息: {} 结束;用时 {} ms", topicName, remark(), message, System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            log.debug("消费【{}】【{}】消息: {} 失败;用时 {} ms", topicName, remark(), message, System.currentTimeMillis() - startTime, e);
            throw e;
        }
    }

    private Type getType() {
        return TypeUtil.getTypeArgument(getClass());
    }

    /**
     * 用于获取主题名称
     *
     * @return 主题名称
     */
    protected abstract String getTopicName();

    /**
     * 用于获取消息处理描述
     *
     * @return 消息处理描述
     */
    protected abstract String remark();

    protected abstract void doMessage(T message);
}
