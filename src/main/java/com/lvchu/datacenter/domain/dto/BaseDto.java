package com.lvchu.datacenter.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lvchu.datacenter.common.annos.Display;
import com.lvchu.datacenter.common.base.BaseEntity;
import com.lvchu.datacenter.common.base.IEntity;
import com.lvchu.datacenter.common.base.PersistStatus;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Transient;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * 数据传输对象的抽象基类。
 *
 * @author wangruiv
 */
public abstract class BaseDto implements Serializable, IEntity {
    @ApiModelProperty(value = "主键", position = 1)
    @Display("主键")
    @Size(max = 40)
    protected String id;

    @ApiModelProperty(value = "删除标识", position = 2, accessMode = ApiModelProperty.AccessMode.READ_ONLY)
    protected Integer dr = 0;

    @ApiModelProperty(value = "时间戳", position = 3, accessMode = ApiModelProperty.AccessMode.READ_ONLY)
    protected Date ts;

    @ApiModelProperty(value = "创建人", position = 4)
    @Display("创建人")
    @Size(max = 20)
    protected String creator;

    @ApiModelProperty(value = "创建时间", position = 5)
    @Size(max = 19)
    protected Date creationTime;

    @ApiModelProperty(value = "修改人", position = 6)
    @Size(max = 20)
    protected String modifier;

    @ApiModelProperty(value = "修改时间", position = 7)
    @Size(max = 19)
    protected Date modifiedTime;

    @ApiModelProperty(value = "持久化状态", position = 8)
    protected String persistStatus = PersistStatus.ADDED;

    /**
     * 数据库中的原值。
     */
    @Transient
    @JsonIgnore
    protected BaseEntity oldValue;

    /**
     * 规则提示信息
     */
    @ApiModelProperty(hidden = true)

    protected String promptMessage;

    public BaseEntity getOldValue() {
        return oldValue;
    }

    public void setOldValue(BaseEntity oldValue) {
        this.oldValue = oldValue;
    }

    public String getPromptMessage() {
        return promptMessage;
    }

    public void setPromptMessage(String promptMessage) {
        this.promptMessage = promptMessage;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Integer getDr() {
        return dr;
    }

    @Override
    public void setDr(Integer dr) {
        this.dr = dr;
    }

    @Override
    public Date getTs() {
        return ts;
    }

    @Override
    public void setTs(Date ts) {
        this.ts = ts;
    }

    @Override
    public String getCreator() {
        return creator;
    }

    @Override
    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Override
    public Date getCreationTime() {
        return creationTime;
    }

    @Override
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String getModifier() {
        return modifier;
    }

    @Override
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    @Override
    public Date getModifiedTime() {
        return modifiedTime;
    }

    @Override
    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @Override
    public String getPersistStatus() {
        return persistStatus;
    }

    @Override
    public void setPersistStatus(String persistStatus) {
        this.persistStatus = persistStatus;
    }
}
