package com.lvchu.datacenter.domain.dto;

import com.lvchu.datacenter.common.config.IntegrationNcConfig;
import com.lvchu.datacenter.common.util.GsonUtil;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 向NC发起的请求数据。
 *
 * @author wangruiv
 */
@Getter
public class ToNcRequestDto {
    private String usercode;

    private String pwd;

    private String datasource;

    private Map<String, Object> data;

    public ToNcRequestDto(IntegrationNcConfig integrationNcConfig) {
        initLoginInfo(integrationNcConfig);
    }

    /**
     * 构造向NC发起的请求数据。
     *
     * @param param 请求数据参数
     * @param businessModule 业务模块
     * @param operation 业务操作
     */
    public void build(Object param, String businessModule, String operation) {
        data = new HashMap<>(4);
        data.put("param", GsonUtil.gsonString(param));
        data.put("businessModule", businessModule);
        data.put("operation", operation);
    }

    /**
     * 初始化登录信息。
     */
    private void initLoginInfo(IntegrationNcConfig integrationNcConfig) {
        // 从配置文件中读取NC的模拟登录信息
        usercode = integrationNcConfig.getUsercode();
        pwd = integrationNcConfig.getPwd();
        datasource = integrationNcConfig.getDatasource();
    }
}
