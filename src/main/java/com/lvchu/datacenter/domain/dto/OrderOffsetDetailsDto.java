//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.lvchu.datacenter.domain.dto;

import com.lvchu.datacenter.common.annos.Display;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.Size;

@Display("B2B销售订单费用明细")
public class OrderOffsetDetailsDto extends BaseDto {
    @Display("销售订单ID")
    @Size(
        max = 40
    )
    private String orderId;
    @Display("销售订单行ID")
    @Size(
        max = 40
    )
    private String orderItemId;
    @Display("冲抵商品")
    @Size(
        max = 40
    )
    private String goodsId;
    private String goodsCode;
    private String goodsName;
    @Display("费用单账户")
    @Size(
        max = 40
    )
    private String feeAccountId;
    private String feeAccountCode;
    private String feeAccountName;
    @Display("账户在该行的总冲抵金额")
    private BigDecimal totalOffsetAmount;
    @Display("冲抵类型")
    private Integer offsetType;
    private Set<OrderOffsetItemDto> orderOffsetItems = new HashSet();

    public OrderOffsetDetailsDto() {
    }

    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }

    public void setOrderItemId(final String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public void setGoodsId(final String goodsId) {
        this.goodsId = goodsId;
    }

    public void setGoodsCode(final String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public void setGoodsName(final String goodsName) {
        this.goodsName = goodsName;
    }

    public void setFeeAccountId(final String feeAccountId) {
        this.feeAccountId = feeAccountId;
    }

    public void setFeeAccountCode(final String feeAccountCode) {
        this.feeAccountCode = feeAccountCode;
    }

    public void setFeeAccountName(final String feeAccountName) {
        this.feeAccountName = feeAccountName;
    }

    public void setTotalOffsetAmount(final BigDecimal totalOffsetAmount) {
        this.totalOffsetAmount = totalOffsetAmount;
    }

    public void setOffsetType(final Integer offsetType) {
        this.offsetType = offsetType;
    }

    public void setOrderOffsetItems(final Set<OrderOffsetItemDto> orderOffsetItems) {
        this.orderOffsetItems = orderOffsetItems;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public String getOrderItemId() {
        return this.orderItemId;
    }

    public String getGoodsId() {
        return this.goodsId;
    }

    public String getGoodsCode() {
        return this.goodsCode;
    }

    public String getGoodsName() {
        return this.goodsName;
    }

    public String getFeeAccountId() {
        return this.feeAccountId;
    }

    public String getFeeAccountCode() {
        return this.feeAccountCode;
    }

    public String getFeeAccountName() {
        return this.feeAccountName;
    }

    public BigDecimal getTotalOffsetAmount() {
        return this.totalOffsetAmount;
    }

    public Integer getOffsetType() {
        return this.offsetType;
    }

    public Set<OrderOffsetItemDto> getOrderOffsetItems() {
        return this.orderOffsetItems;
    }

    @Override
    public String toString() {
        return "OrderOffsetDetailsDto(orderId=" + this.getOrderId() + ", orderItemId=" + this.getOrderItemId() + ", goodsId=" + this.getGoodsId() + ", goodsCode=" + this.getGoodsCode() + ", goodsName=" + this.getGoodsName() + ", feeAccountId=" + this.getFeeAccountId() + ", feeAccountCode=" + this.getFeeAccountCode() + ", feeAccountName=" + this.getFeeAccountName() + ", totalOffsetAmount=" + this.getTotalOffsetAmount() + ", offsetType=" + this.getOffsetType() + ", orderOffsetItems=" + this.getOrderOffsetItems() + ")";
    }
}
