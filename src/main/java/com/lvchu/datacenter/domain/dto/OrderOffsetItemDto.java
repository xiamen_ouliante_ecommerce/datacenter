//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.lvchu.datacenter.domain.dto;

import com.lvchu.datacenter.common.annos.Display;
import java.math.BigDecimal;
import javax.validation.constraints.Size;

@Display("B2B销售订单费用单明细")
public class OrderOffsetItemDto extends BaseDto {
    @Display("费用单ID")
    @Size(
        max = 40
    )
    private String feeId;
    @Display("费用单号")
    @Size(
        max = 50
    )
    private String feeCode;
    @Display("费用单行ID")
    @Size(
        max = 40
    )
    private String feeItemId;
    @Display("冲抵金额")
    private BigDecimal offsetAmount;
    @Display("冲抵明细主键")
    @Size(
        max = 40
    )
    private String offsetDetailId;

    public OrderOffsetItemDto() {
    }

    public String getFeeId() {
        return this.feeId;
    }

    public String getFeeCode() {
        return this.feeCode;
    }

    public String getFeeItemId() {
        return this.feeItemId;
    }

    public BigDecimal getOffsetAmount() {
        return this.offsetAmount;
    }

    public String getOffsetDetailId() {
        return this.offsetDetailId;
    }

    public void setFeeId(final String feeId) {
        this.feeId = feeId;
    }

    public void setFeeCode(final String feeCode) {
        this.feeCode = feeCode;
    }

    public void setFeeItemId(final String feeItemId) {
        this.feeItemId = feeItemId;
    }

    public void setOffsetAmount(final BigDecimal offsetAmount) {
        this.offsetAmount = offsetAmount;
    }

    public void setOffsetDetailId(final String offsetDetailId) {
        this.offsetDetailId = offsetDetailId;
    }

    @Override
    public String toString() {
        return "OrderOffsetItemDto(feeId=" + this.getFeeId() + ", feeCode=" + this.getFeeCode() + ", feeItemId=" + this.getFeeItemId() + ", offsetAmount=" + this.getOffsetAmount() + ", offsetDetailId=" + this.getOffsetDetailId() + ")";
    }
}
