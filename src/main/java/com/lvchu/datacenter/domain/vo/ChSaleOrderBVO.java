package com.lvchu.datacenter.domain.vo;

import com.google.common.base.MoreObjects;
import nc.vo.pub.SuperVO;
import nc.vo.pub.lang.UFDate;
import nc.vo.pub.lang.UFDateTime;
import nc.vo.pub.lang.UFDouble;

public class ChSaleOrderBVO extends SuperVO {
    /**
     * 原金额
     */
    private UFDouble amount;
    /**
     * 选配结果主键
     */
    private String basegoodsoptid;
    /**
     * 选配结果展示值
     */
    private String basegoodsoptvalue;
    /**
     * 单价
     */
    private UFDouble baseprice;
    /**
     * 批号
     */
    private String batchcode;
    /**
     * 关闭原因
     */
    private String closereason;
    /**
     * 单位换算率
     */
    private UFDouble conversionrate;
    /**
     * 累计协同数量
     */
    private UFDouble coordinatenum;
    /**
     * 行号
     */
    private String crowno;
    /**
     * 币种
     */
    private String currency;
    /**
     * 成交金额
     */
    private UFDouble dealamount;
    /**
     * 成交价
     */
    private UFDouble dealprice;
    /**
     * 发货库存组织
     */
    private String deliveryinvorg;
    /**
     * 累计发货数量
     */
    private UFDouble deliverynum;
    /**
     * 发货仓库
     */
    private String deliverywarehouse;
    /**
     * 扩展字段1
     */
    private String ext01;
    /**
     * 扩展字段2
     */
    private String ext02;
    /**
     * 扩展字段3
     */
    private String ext03;
    /**
     * 扩展字段4
     */
    private String ext04;
    /**
     * 扩展字段5
     */
    private String ext05;
    /**
     * 扩展字段6
     */
    private String ext06;
    /**
     * 扩展字段7
     */
    private String ext07;
    /**
     * 扩展字段8
     */
    private String ext08;
    /**
     * 扩展字段9
     */
    private String ext09;
    /**
     * 扩展字段10
     */
    private String ext10;
    /**
     * 扩展字段11
     */
    private String ext11;
    /**
     * 扩展字段12
     */
    private String ext12;
    /**
     * 扩展字段13
     */
    private String ext13;
    /**
     * 扩展字段14
     */
    private String ext14;
    /**
     * 扩展字段15
     */
    private String ext15;
    /**
     * 商品分类
     */
    private String goodscategoryid;
    /**
     * 商品ID
     */
    private String goodsid;

    /**
     * 商品编码
     */
    private String goodscode;

    /**
     * 主记账单价
     */
    private UFDouble goossupplementprice;
    /**
     * 关闭
     */
    private Integer isclose;
    /**
     * 是否为赠品
     */
    private Integer isgift;
    /**
     * 是否货补
     */
    private Integer isgoodssupplement;
    /**
     * 主数量
     */
    private UFDouble mainnum;
    /**
     * 主数量单位
     */
    private String mainnumunit;
    /**
     * 冲抵金额
     */
    private UFDouble offsetamount;
    /**
     * 表体主键
     */
    private String orderbid;
    /**
     * 订货数量
     */
    private UFDouble ordernum;
    /**
     * 订货单位
     */
    private String ordernumunit;
    /**
     * 计划发货日期
     */
    private UFDate plandeliverydate;
    /**
     * 产品ID
     */
    private String productid;
    /**
     * 产品线
     */
    private String productlineid;
    /**
     * 所属项目
     */
    private String project;
    /**
     * 促销金额
     */
    private UFDouble promamount;
    /**
     * 促销活动
     */
    private String promotinid;
    /**
     * 促销折扣价格
     */
    private UFDouble promprice;
    /**
     * 累计退款数量
     */
    private UFDouble refundnum;
    /**
     * 备注
     */
    private String remark;
    /**
     * 累计补货数量
     */
    private UFDouble replenishnum;
    /**
     * 累计退货数量
     */
    private UFDouble returnnum;
    /**
     * 退货类型
     */
    private String returntype;
    /**
     * 行合计体积
     */
    private UFDouble rowvolume;
    /**
     * 行合计重量
     */
    private UFDouble rowweight;
    /**
     * 行合计净重量
     */
    private UFDouble rownetweight;
    /**
     * 基准折扣价格
     */
    private UFDouble saleprice;
    /**
     * 结算财务组织
     */
    private String settlefinancialorg;
    /**
     * 累计签收数量
     */
    private String signnum;
    /**
     * 来源订单单据类型
     */
    private String srcorderbilltype;
    /**
     * 来源订单号
     */
    private String srcordercode;
    /**
     * 来源订单主键
     */
    private String srcorderid;
    /**
     * 来源订单行主键
     */
    private String srcorderitemid;
    /**
     * 来源订单交易类型
     */
    private String srcordertrantype;
    /**
     * 关联要货单号
     */
    private String srcreqordercode;
    /**
     * 关联要货单主键
     */
    private String srcreqorderid;
    /**
     * 关联要货单行主键
     */
    private String srcreqorderitemid;
    /**
     * 累计入库数量
     */
    private UFDouble stockinnum;
    /**
     * 累计出库数量
     */
    private UFDouble stockoutnum;
    /**
     * 供应商
     */
    private String supplier;
    /**
     * 实际退货金额
     */
    private UFDouble totalreturnamount;
    /**
     * 时间戳
     */
    private UFDateTime ts;
    /**
     * 体积
     */
    private UFDouble volume;
    /**
     * 体积单位
     */
    private String volumeunit;
    /**
     * 重量
     */
    private UFDouble weight;
    /**
     * 净重量
     */
    private UFDouble netweight;
    /**
     * 重量单位
     */
    private String weightunit;


    public UFDouble getAmount() {
        return amount;
    }

    public void setAmount(UFDouble amount) {
        this.amount = amount;
    }

    public String getBasegoodsoptid() {
        return basegoodsoptid;
    }

    public void setBasegoodsoptid(String basegoodsoptid) {
        this.basegoodsoptid = basegoodsoptid;
    }

    public String getBasegoodsoptvalue() {
        return basegoodsoptvalue;
    }

    public void setBasegoodsoptvalue(String basegoodsoptvalue) {
        this.basegoodsoptvalue = basegoodsoptvalue;
    }

    public UFDouble getBaseprice() {
        return baseprice;
    }

    public void setBaseprice(UFDouble baseprice) {
        this.baseprice = baseprice;
    }

    public String getBatchcode() {
        return batchcode;
    }

    public void setBatchcode(String batchcode) {
        this.batchcode = batchcode;
    }

    public String getClosereason() {
        return closereason;
    }

    public void setClosereason(String closereason) {
        this.closereason = closereason;
    }

    public UFDouble getConversionrate() {
        return conversionrate;
    }

    public void setConversionrate(UFDouble conversionrate) {
        this.conversionrate = conversionrate;
    }

    public UFDouble getCoordinatenum() {
        return coordinatenum;
    }

    public void setCoordinatenum(UFDouble coordinatenum) {
        this.coordinatenum = coordinatenum;
    }

    public String getCrowno() {
        return crowno;
    }

    public void setCrowno(String crowno) {
        this.crowno = crowno;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public UFDouble getDealamount() {
        return dealamount;
    }

    public void setDealamount(UFDouble dealamount) {
        this.dealamount = dealamount;
    }

    public UFDouble getDealprice() {
        return dealprice;
    }

    public void setDealprice(UFDouble dealprice) {
        this.dealprice = dealprice;
    }

    public String getDeliveryinvorg() {
        return deliveryinvorg;
    }

    public void setDeliveryinvorg(String deliveryinvorg) {
        this.deliveryinvorg = deliveryinvorg;
    }

    public UFDouble getDeliverynum() {
        return deliverynum;
    }

    public void setDeliverynum(UFDouble deliverynum) {
        this.deliverynum = deliverynum;
    }

    public String getDeliverywarehouse() {
        return deliverywarehouse;
    }

    public void setDeliverywarehouse(String deliverywarehouse) {
        this.deliverywarehouse = deliverywarehouse;
    }

    public String getExt01() {
        return ext01;
    }

    public void setExt01(String ext01) {
        this.ext01 = ext01;
    }

    public String getExt02() {
        return ext02;
    }

    public void setExt02(String ext02) {
        this.ext02 = ext02;
    }

    public String getExt03() {
        return ext03;
    }

    public void setExt03(String ext03) {
        this.ext03 = ext03;
    }

    public String getExt04() {
        return ext04;
    }

    public void setExt04(String ext04) {
        this.ext04 = ext04;
    }

    public String getExt05() {
        return ext05;
    }

    public void setExt05(String ext05) {
        this.ext05 = ext05;
    }

    public String getExt06() {
        return ext06;
    }

    public void setExt06(String ext06) {
        this.ext06 = ext06;
    }

    public String getExt07() {
        return ext07;
    }

    public void setExt07(String ext07) {
        this.ext07 = ext07;
    }

    public String getExt08() {
        return ext08;
    }

    public void setExt08(String ext08) {
        this.ext08 = ext08;
    }

    public String getExt09() {
        return ext09;
    }

    public void setExt09(String ext09) {
        this.ext09 = ext09;
    }

    public String getExt10() {
        return ext10;
    }

    public void setExt10(String ext10) {
        this.ext10 = ext10;
    }

    public String getExt11() {
        return ext11;
    }

    public void setExt11(String ext11) {
        this.ext11 = ext11;
    }

    public String getExt12() {
        return ext12;
    }

    public void setExt12(String ext12) {
        this.ext12 = ext12;
    }

    public String getExt13() {
        return ext13;
    }

    public void setExt13(String ext13) {
        this.ext13 = ext13;
    }

    public String getExt14() {
        return ext14;
    }

    public void setExt14(String ext14) {
        this.ext14 = ext14;
    }

    public String getExt15() {
        return ext15;
    }

    public void setExt15(String ext15) {
        this.ext15 = ext15;
    }

    public String getGoodscategoryid() {
        return goodscategoryid;
    }

    public void setGoodscategoryid(String goodscategoryid) {
        this.goodscategoryid = goodscategoryid;
    }

    public String getGoodsid() {
        return goodsid;
    }

    public void setGoodsid(String goodsid) {
        this.goodsid = goodsid;
    }

    public UFDouble getGoossupplementprice() {
        return goossupplementprice;
    }

    public void setGoossupplementprice(UFDouble goossupplementprice) {
        this.goossupplementprice = goossupplementprice;
    }

    public Integer getIsclose() {
        return isclose;
    }

    public void setIsclose(Integer isclose) {
        this.isclose = isclose;
    }

    public Integer getIsgift() {
        return isgift;
    }

    public void setIsgift(Integer isgift) {
        this.isgift = isgift;
    }

    public Integer getIsgoodssupplement() {
        return isgoodssupplement;
    }

    public void setIsgoodssupplement(Integer isgoodssupplement) {
        this.isgoodssupplement = isgoodssupplement;
    }

    public UFDouble getMainnum() {
        return mainnum;
    }

    public void setMainnum(UFDouble mainnum) {
        this.mainnum = mainnum;
    }

    public String getMainnumunit() {
        return mainnumunit;
    }

    public void setMainnumunit(String mainnumunit) {
        this.mainnumunit = mainnumunit;
    }

    public UFDouble getOffsetamount() {
        return offsetamount;
    }

    public void setOffsetamount(UFDouble offsetamount) {
        this.offsetamount = offsetamount;
    }

    public String getOrderbid() {
        return orderbid;
    }

    public void setOrderbid(String orderbid) {
        this.orderbid = orderbid;
    }

    public UFDouble getOrdernum() {
        return ordernum;
    }

    public void setOrdernum(UFDouble ordernum) {
        this.ordernum = ordernum;
    }

    public String getOrdernumunit() {
        return ordernumunit;
    }

    public void setOrdernumunit(String ordernumunit) {
        this.ordernumunit = ordernumunit;
    }

    public UFDate getPlandeliverydate() {
        return plandeliverydate;
    }

    public void setPlandeliverydate(UFDate plandeliverydate) {
        this.plandeliverydate = plandeliverydate;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductlineid() {
        return productlineid;
    }

    public void setProductlineid(String productlineid) {
        this.productlineid = productlineid;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public UFDouble getPromamount() {
        return promamount;
    }

    public void setPromamount(UFDouble promamount) {
        this.promamount = promamount;
    }

    public String getPromotinid() {
        return promotinid;
    }

    public void setPromotinid(String promotinid) {
        this.promotinid = promotinid;
    }

    public UFDouble getPromprice() {
        return promprice;
    }

    public void setPromprice(UFDouble promprice) {
        this.promprice = promprice;
    }

    public UFDouble getRefundnum() {
        return refundnum;
    }

    public void setRefundnum(UFDouble refundnum) {
        this.refundnum = refundnum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public UFDouble getReplenishnum() {
        return replenishnum;
    }

    public void setReplenishnum(UFDouble replenishnum) {
        this.replenishnum = replenishnum;
    }

    public UFDouble getReturnnum() {
        return returnnum;
    }

    public void setReturnnum(UFDouble returnnum) {
        this.returnnum = returnnum;
    }

    public String getReturntype() {
        return returntype;
    }

    public void setReturntype(String returntype) {
        this.returntype = returntype;
    }

    public UFDouble getRowvolume() {
        return rowvolume;
    }

    public void setRowvolume(UFDouble rowvolume) {
        this.rowvolume = rowvolume;
    }

    public UFDouble getRowweight() {
        return rowweight;
    }

    public void setRowweight(UFDouble rowweight) {
        this.rowweight = rowweight;
    }

    public UFDouble getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(UFDouble saleprice) {
        this.saleprice = saleprice;
    }

    public String getSettlefinancialorg() {
        return settlefinancialorg;
    }

    public void setSettlefinancialorg(String settlefinancialorg) {
        this.settlefinancialorg = settlefinancialorg;
    }

    public String getSignnum() {
        return signnum;
    }

    public void setSignnum(String signnum) {
        this.signnum = signnum;
    }

    public String getSrcorderbilltype() {
        return srcorderbilltype;
    }

    public void setSrcorderbilltype(String srcorderbilltype) {
        this.srcorderbilltype = srcorderbilltype;
    }

    public String getSrcordercode() {
        return srcordercode;
    }

    public void setSrcordercode(String srcordercode) {
        this.srcordercode = srcordercode;
    }

    public String getSrcorderid() {
        return srcorderid;
    }

    public void setSrcorderid(String srcorderid) {
        this.srcorderid = srcorderid;
    }

    public String getSrcorderitemid() {
        return srcorderitemid;
    }

    public void setSrcorderitemid(String srcorderitemid) {
        this.srcorderitemid = srcorderitemid;
    }

    public String getSrcordertrantype() {
        return srcordertrantype;
    }

    public void setSrcordertrantype(String srcordertrantype) {
        this.srcordertrantype = srcordertrantype;
    }

    public String getSrcreqordercode() {
        return srcreqordercode;
    }

    public void setSrcreqordercode(String srcreqordercode) {
        this.srcreqordercode = srcreqordercode;
    }

    public String getSrcreqorderid() {
        return srcreqorderid;
    }

    public void setSrcreqorderid(String srcreqorderid) {
        this.srcreqorderid = srcreqorderid;
    }

    public String getSrcreqorderitemid() {
        return srcreqorderitemid;
    }

    public void setSrcreqorderitemid(String srcreqorderitemid) {
        this.srcreqorderitemid = srcreqorderitemid;
    }

    public UFDouble getStockinnum() {
        return stockinnum;
    }

    public void setStockinnum(UFDouble stockinnum) {
        this.stockinnum = stockinnum;
    }

    public UFDouble getStockoutnum() {
        return stockoutnum;
    }

    public void setStockoutnum(UFDouble stockoutnum) {
        this.stockoutnum = stockoutnum;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public UFDouble getTotalreturnamount() {
        return totalreturnamount;
    }

    public void setTotalreturnamount(UFDouble totalreturnamount) {
        this.totalreturnamount = totalreturnamount;
    }

    public UFDateTime getTs() {
        return ts;
    }

    public void setTs(UFDateTime ts) {
        this.ts = ts;
    }

    public UFDouble getVolume() {
        return volume;
    }

    public void setVolume(UFDouble volume) {
        this.volume = volume;
    }

    public String getVolumeunit() {
        return volumeunit;
    }

    public void setVolumeunit(String volumeunit) {
        this.volumeunit = volumeunit;
    }

    public UFDouble getWeight() {
        return weight;
    }

    public void setWeight(UFDouble weight) {
        this.weight = weight;
    }

    public String getWeightunit() {
        return weightunit;
    }

    public void setWeightunit(String weightunit) {
        this.weightunit = weightunit;
    }

    public String getGoodscode() {
        return goodscode;
    }

    public void setGoodscode(String goodscode) {
        this.goodscode = goodscode;
    }

    public UFDouble getRownetweight() {
        return rownetweight;
    }

    public void setRownetweight(UFDouble rownetweight) {
        this.rownetweight = rownetweight;
    }

    public UFDouble getNetweight() {
        return netweight;
    }

    public void setNetweight(UFDouble netweight) {
        this.netweight = netweight;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("amount", amount)
                .add("basegoodsoptid", basegoodsoptid)
                .add("basegoodsoptvalue", basegoodsoptvalue)
                .add("baseprice", baseprice)
                .add("batchcode", batchcode)
                .add("closereason", closereason)
                .add("conversionrate", conversionrate)
                .add("coordinatenum", coordinatenum)
                .add("crowno", crowno)
                .add("currency", currency)
                .add("dealamount", dealamount)
                .add("dealprice", dealprice)
                .add("deliveryinvorg", deliveryinvorg)
                .add("deliverynum", deliverynum)
                .add("deliverywarehouse", deliverywarehouse)
                .add("ext01", ext01)
                .add("ext02", ext02)
                .add("ext03", ext03)
                .add("ext04", ext04)
                .add("ext05", ext05)
                .add("ext06", ext06)
                .add("ext07", ext07)
                .add("ext08", ext08)
                .add("ext09", ext09)
                .add("ext10", ext10)
                .add("ext11", ext11)
                .add("ext12", ext12)
                .add("ext13", ext13)
                .add("ext14", ext14)
                .add("ext15", ext15)
                .add("goodscategoryid", goodscategoryid)
                .add("goodsid", goodsid)
                .add("goodscode", goodscode)
                .add("goossupplementprice", goossupplementprice)
                .add("isclose", isclose)
                .add("isgift", isgift)
                .add("isgoodssupplement", isgoodssupplement)
                .add("mainnum", mainnum)
                .add("mainnumunit", mainnumunit)
                .add("offsetamount", offsetamount)
                .add("orderbid", orderbid)
                .add("ordernum", ordernum)
                .add("ordernumunit", ordernumunit)
                .add("plandeliverydate", plandeliverydate)
                .add("productid", productid)
                .add("productlineid", productlineid)
                .add("project", project)
                .add("promamount", promamount)
                .add("promotinid", promotinid)
                .add("promprice", promprice)
                .add("refundnum", refundnum)
                .add("remark", remark)
                .add("replenishnum", replenishnum)
                .add("returnnum", returnnum)
                .add("returntype", returntype)
                .add("rowvolume", rowvolume)
                .add("rowweight", rowweight)
                .add("rownetweight", rownetweight)
                .add("saleprice", saleprice)
                .add("settlefinancialorg", settlefinancialorg)
                .add("signnum", signnum)
                .add("srcorderbilltype", srcorderbilltype)
                .add("srcordercode", srcordercode)
                .add("srcorderid", srcorderid)
                .add("srcorderitemid", srcorderitemid)
                .add("srcordertrantype", srcordertrantype)
                .add("srcreqordercode", srcreqordercode)
                .add("srcreqorderid", srcreqorderid)
                .add("srcreqorderitemid", srcreqorderitemid)
                .add("stockinnum", stockinnum)
                .add("stockoutnum", stockoutnum)
                .add("supplier", supplier)
                .add("totalreturnamount", totalreturnamount)
                .add("ts", ts)
                .add("volume", volume)
                .add("volumeunit", volumeunit)
                .add("weight", weight)
                .add("netweight", netweight)
                .add("weightunit", weightunit)
                .toString();
    }
}