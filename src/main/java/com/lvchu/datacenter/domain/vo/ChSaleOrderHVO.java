package com.lvchu.datacenter.domain.vo;

import com.google.common.base.MoreObjects;
import nc.vo.pub.SuperVO;
import nc.vo.pub.lang.UFDate;
import nc.vo.pub.lang.UFDateTime;
import nc.vo.pub.lang.UFDouble;

public class ChSaleOrderHVO extends SuperVO {
    /**
     * 账期
     */
    private String accountperiod;
    /**
     * 关闭人
     */
    private String closer;
    /**
     * 关闭/打开原因
     */
    private String closereason;
    /**
     * 关闭时间
     */
    private UFDateTime closetime;
    /**
     * 主键
     */
    private String corderid;
    /**
     * 货补费用单类型
     */
    private String costtype;
    /**
     * creationTime
     */
    private UFDateTime creationtime;
    /**
     * creator
     */
    private String creator;
    /**
     * 币种
     */
    private String currency;
    /**
     * 客户
     */
    private String customer;
    /**
     * 交货日期
     */
    private UFDate deliverydate;
    /**
     * 扩展字段1
     */
    private String ext01;
    /**
     * 扩展字段2
     */
    private String ext02;
    /**
     * 扩展字段3
     */
    private String ext03;
    /**
     * 扩展字段4
     */
    private String ext04;
    /**
     * 扩展字段5
     */
    private String ext05;
    /**
     * 扩展字段6
     */
    private String ext06;
    /**
     * 扩展字段7
     */
    private String ext07;
    /**
     * 扩展字段8
     */
    private String ext08;
    /**
     * 扩展字段9
     */
    private String ext09;
    /**
     * 扩展字段10
     */
    private String ext10;
    /**
     * 扩展字段11
     */
    private String ext11;
    /**
     * 扩展字段12
     */
    private String ext12;
    /**
     * 扩展字段13
     */
    private String ext13;
    /**
     * 扩展字段14
     */
    private String ext14;
    /**
     * 扩展字段15
     */
    private String ext15;
    /**
     * 关闭
     */
    private Integer isclose;
    /**
     * 物流快递公司
     */
    private String logistics;
    /**
     * 物流单号
     */
    private String logisticsbillcode;
    /**
     * 市场区域
     */
    private String marketarea;
    /**
     * modifiedTime
     */
    private UFDateTime modifiedtime;
    /**
     * modifier
     */
    private String modifier;
    /**
     * 费用冲抵金额
     */
    private UFDouble offsetamount;
    /**
     * 订单编码
     */
    private String ordercode;
    /**
     * 订单日期
     */
    private UFDate orderdate;
    /**
     * 订单来源
     */
    private String ordersource;
    /**
     * 订单状态
     */
    private String orderstatus;
    /**
     * 订单类型
     */
    private String ordertype;
    /**
     * 原单订单号
     */
    private String originalordercode;
    /**
     * 集团
     */
    private String pk_group;
    /**
     * 客户收货地址
     */
    private String pk_receiveraddress;
    /**
     * 促销金额
     */
    private UFDouble promamount;
    /**
     * 备注
     */
    private String remark;
    /**
     * 销售模式
     */
    private String alemodel;
    /**
     * 销售组织
     */
    private String saleorg;
    /**
     * 销售部门
     */
    private String salesdept;
    /**
     * 客户经理
     */
    private String salesmanager;
    /**
     * 结算财务组织
     */
    private String settlefinancialorg;
    /**
     * 结算方式
     */
    private String settlemode;
    /**
     * 来源订单号
     */
    private String srcordercode;
    /**
     * 来源订单主键
     */
    private String srcorderid;
    /**
     * 关联要货单订单号
     */
    private String srcreqordercode;
    /**
     * 关联要货单单主键
     */
    private String srcreqorderid;
    /**
     * 原金额
     */
    private UFDouble totalamount;
    /**
     * 总成交金额
     */
    private UFDouble totaldealamount;
    /**
     * 总数量
     */
    private UFDouble totalnum;
    /**
     * 总实际退货金额
     */
    private UFDouble totalreturnamount;
    /**
     * 总体积
     */
    private UFDouble totalvolume;
    /**
     * 总重量
     */
    private UFDouble totalweight;
    /**
     * 运输方式
     */
    private String transportmode;
    /**
     * 时间戳
     */
    private UFDateTime ts;

    public String getAccountperiod() {
        return accountperiod;
    }

    public void setAccountperiod(String accountperiod) {
        this.accountperiod = accountperiod;
    }

    public String getCloser() {
        return closer;
    }

    public void setCloser(String closer) {
        this.closer = closer;
    }

    public String getClosereason() {
        return closereason;
    }

    public void setClosereason(String closereason) {
        this.closereason = closereason;
    }

    public UFDateTime getClosetime() {
        return closetime;
    }

    public void setClosetime(UFDateTime closetime) {
        this.closetime = closetime;
    }

    public String getCorderid() {
        return corderid;
    }

    public void setCorderid(String corderid) {
        this.corderid = corderid;
    }

    public String getCosttype() {
        return costtype;
    }

    public void setCosttype(String costtype) {
        this.costtype = costtype;
    }

    public UFDateTime getCreationtime() {
        return creationtime;
    }

    public void setCreationtime(UFDateTime creationtime) {
        this.creationtime = creationtime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public UFDate getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(UFDate deliverydate) {
        this.deliverydate = deliverydate;
    }

    public String getExt01() {
        return ext01;
    }

    public void setExt01(String ext01) {
        this.ext01 = ext01;
    }

    public String getExt02() {
        return ext02;
    }

    public void setExt02(String ext02) {
        this.ext02 = ext02;
    }

    public String getExt03() {
        return ext03;
    }

    public void setExt03(String ext03) {
        this.ext03 = ext03;
    }

    public String getExt04() {
        return ext04;
    }

    public void setExt04(String ext04) {
        this.ext04 = ext04;
    }

    public String getExt05() {
        return ext05;
    }

    public void setExt05(String ext05) {
        this.ext05 = ext05;
    }

    public String getExt06() {
        return ext06;
    }

    public void setExt06(String ext06) {
        this.ext06 = ext06;
    }

    public String getExt07() {
        return ext07;
    }

    public void setExt07(String ext07) {
        this.ext07 = ext07;
    }

    public String getExt08() {
        return ext08;
    }

    public void setExt08(String ext08) {
        this.ext08 = ext08;
    }

    public String getExt09() {
        return ext09;
    }

    public void setExt09(String ext09) {
        this.ext09 = ext09;
    }

    public String getExt10() {
        return ext10;
    }

    public void setExt10(String ext10) {
        this.ext10 = ext10;
    }

    public String getExt11() {
        return ext11;
    }

    public void setExt11(String ext11) {
        this.ext11 = ext11;
    }

    public String getExt12() {
        return ext12;
    }

    public void setExt12(String ext12) {
        this.ext12 = ext12;
    }

    public String getExt13() {
        return ext13;
    }

    public void setExt13(String ext13) {
        this.ext13 = ext13;
    }

    public String getExt14() {
        return ext14;
    }

    public void setExt14(String ext14) {
        this.ext14 = ext14;
    }

    public String getExt15() {
        return ext15;
    }

    public void setExt15(String ext15) {
        this.ext15 = ext15;
    }

    public Integer getIsclose() {
        return isclose;
    }

    public void setIsclose(Integer isclose) {
        this.isclose = isclose;
    }

    public String getLogistics() {
        return logistics;
    }

    public void setLogistics(String logistics) {
        this.logistics = logistics;
    }

    public String getLogisticsbillcode() {
        return logisticsbillcode;
    }

    public void setLogisticsbillcode(String logisticsbillcode) {
        this.logisticsbillcode = logisticsbillcode;
    }

    public String getMarketarea() {
        return marketarea;
    }

    public void setMarketarea(String marketarea) {
        this.marketarea = marketarea;
    }

    public UFDateTime getModifiedtime() {
        return modifiedtime;
    }

    public void setModifiedtime(UFDateTime modifiedtime) {
        this.modifiedtime = modifiedtime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public UFDouble getOffsetamount() {
        return offsetamount;
    }

    public void setOffsetamount(UFDouble offsetamount) {
        this.offsetamount = offsetamount;
    }

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public UFDate getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(UFDate orderdate) {
        this.orderdate = orderdate;
    }

    public String getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(String ordersource) {
        this.ordersource = ordersource;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    public String getOrdertype() {
        return ordertype;
    }

    public void setOrdertype(String ordertype) {
        this.ordertype = ordertype;
    }

    public String getOriginalordercode() {
        return originalordercode;
    }

    public void setOriginalordercode(String originalordercode) {
        this.originalordercode = originalordercode;
    }

    public String getPk_group() {
        return pk_group;
    }

    public void setPk_group(String pk_group) {
        this.pk_group = pk_group;
    }

    public String getPk_receiveraddress() {
        return pk_receiveraddress;
    }

    public void setPk_receiveraddress(String pk_receiveraddress) {
        this.pk_receiveraddress = pk_receiveraddress;
    }

    public UFDouble getPromamount() {
        return promamount;
    }

    public void setPromamount(UFDouble promamount) {
        this.promamount = promamount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAlemodel() {
        return alemodel;
    }

    public void setAlemodel(String alemodel) {
        this.alemodel = alemodel;
    }

    public String getSaleorg() {
        return saleorg;
    }

    public void setSaleorg(String saleorg) {
        this.saleorg = saleorg;
    }

    public String getSalesdept() {
        return salesdept;
    }

    public void setSalesdept(String salesdept) {
        this.salesdept = salesdept;
    }

    public String getSalesmanager() {
        return salesmanager;
    }

    public void setSalesmanager(String salesmanager) {
        this.salesmanager = salesmanager;
    }

    public String getSettlefinancialorg() {
        return settlefinancialorg;
    }

    public void setSettlefinancialorg(String settlefinancialorg) {
        this.settlefinancialorg = settlefinancialorg;
    }

    public String getSettlemode() {
        return settlemode;
    }

    public void setSettlemode(String settlemode) {
        this.settlemode = settlemode;
    }

    public String getSrcordercode() {
        return srcordercode;
    }

    public void setSrcordercode(String srcordercode) {
        this.srcordercode = srcordercode;
    }

    public String getSrcorderid() {
        return srcorderid;
    }

    public void setSrcorderid(String srcorderid) {
        this.srcorderid = srcorderid;
    }

    public String getSrcreqordercode() {
        return srcreqordercode;
    }

    public void setSrcreqordercode(String srcreqordercode) {
        this.srcreqordercode = srcreqordercode;
    }

    public String getSrcreqorderid() {
        return srcreqorderid;
    }

    public void setSrcreqorderid(String srcreqorderid) {
        this.srcreqorderid = srcreqorderid;
    }

    public UFDouble getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(UFDouble totalamount) {
        this.totalamount = totalamount;
    }

    public UFDouble getTotaldealamount() {
        return totaldealamount;
    }

    public void setTotaldealamount(UFDouble totaldealamount) {
        this.totaldealamount = totaldealamount;
    }

    public UFDouble getTotalnum() {
        return totalnum;
    }

    public void setTotalnum(UFDouble totalnum) {
        this.totalnum = totalnum;
    }

    public UFDouble getTotalreturnamount() {
        return totalreturnamount;
    }

    public void setTotalreturnamount(UFDouble totalreturnamount) {
        this.totalreturnamount = totalreturnamount;
    }

    public UFDouble getTotalvolume() {
        return totalvolume;
    }

    public void setTotalvolume(UFDouble totalvolume) {
        this.totalvolume = totalvolume;
    }

    public UFDouble getTotalweight() {
        return totalweight;
    }

    public void setTotalweight(UFDouble totalweight) {
        this.totalweight = totalweight;
    }

    public String getTransportmode() {
        return transportmode;
    }

    public void setTransportmode(String transportmode) {
        this.transportmode = transportmode;
    }

    public UFDateTime getTs() {
        return ts;
    }

    public void setTs(UFDateTime ts) {
        this.ts = ts;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("accountperiod", accountperiod)
                .add("closer", closer)
                .add("closereason", closereason)
                .add("closetime", closetime)
                .add("corderid", corderid)
                .add("costtype", costtype)
                .add("creationtime", creationtime)
                .add("creator", creator)
                .add("currency", currency)
                .add("customer", customer)
                .add("deliverydate", deliverydate)
                .add("ext01", ext01)
                .add("ext02", ext02)
                .add("ext03", ext03)
                .add("ext04", ext04)
                .add("ext05", ext05)
                .add("ext06", ext06)
                .add("ext07", ext07)
                .add("ext08", ext08)
                .add("ext09", ext09)
                .add("ext10", ext10)
                .add("ext11", ext11)
                .add("ext12", ext12)
                .add("ext13", ext13)
                .add("ext14", ext14)
                .add("ext15", ext15)
                .add("isclose", isclose)
                .add("logistics", logistics)
                .add("logisticsbillcode", logisticsbillcode)
                .add("marketarea", marketarea)
                .add("modifiedtime", modifiedtime)
                .add("modifier", modifier)
                .add("offsetamount", offsetamount)
                .add("ordercode", ordercode)
                .add("orderdate", orderdate)
                .add("ordersource", ordersource)
                .add("orderstatus", orderstatus)
                .add("ordertype", ordertype)
                .add("originalordercode", originalordercode)
                .add("pk_group", pk_group)
                .add("pk_receiveraddress", pk_receiveraddress)
                .add("promamount", promamount)
                .add("remark", remark)
                .add("alemodel", alemodel)
                .add("saleorg", saleorg)
                .add("salesdept", salesdept)
                .add("salesmanager", salesmanager)
                .add("settlefinancialorg", settlefinancialorg)
                .add("settlemode", settlemode)
                .add("srcordercode", srcordercode)
                .add("srcorderid", srcorderid)
                .add("srcreqordercode", srcreqordercode)
                .add("srcreqorderid", srcreqorderid)
                .add("totalamount", totalamount)
                .add("totaldealamount", totaldealamount)
                .add("totalnum", totalnum)
                .add("totalreturnamount", totalreturnamount)
                .add("totalvolume", totalvolume)
                .add("totalweight", totalweight)
                .add("transportmode", transportmode)
                .add("ts", ts)
                .toString();
    }
}