package com.lvchu.datacenter.domain.vo;

import java.io.Serializable;
import java.util.List;

import com.google.common.base.MoreObjects;
import lombok.Data;

@Data
public class SaleOrderMessageVO implements Serializable {
    private static final long serialVersionUID = -4709768115116429826L;

    private ChSaleOrderHVO saleOrderHVO;

    private List<ChSaleOrderBVO> saleOrderBVOs;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("saleOrderHVO", saleOrderHVO)
                .add("saleOrderBVOs", saleOrderBVOs)
                .toString();
    }
}
